```
[
    {
        "refs": [
            "tag: 1.0.0-b.4",
            "origin/dvt.xyz"
        ],
        "hash": "4c106c65ef3a92e5760fe7379300f9478853f0f7",
        "hashAbbrev": "4c106c65",
        "tree": "2283acd8f4019a7119ac303ed882e5d32424b127",
        "treeAbbrev": "2283acd8",
        "parents": [
            "fe2c48aad4b85311b4a19958559687c70b596f63"
        ],
        "parentsAbbrev": [
            "fe2c48aa"
        ],
        "author": {
            "name": "user",
            "email": "user@email.com",
            "timestamp": 1656708382000
        },
        "committer": {
            "name": "user",
            "email": "user@email.com",
            "timestamp": 1656708382000
        },
        "subject": "jenkins-1.0.0-b.4 [ci skip]",
        "body": "",
        "notes": "",
        "stats": [
            {
                "additions": 1,
                "deletions": 1,
                "file": "package.json"
            },
            {
                "additions": 1,
                "deletions": 1,
                "file": "src/app/test/file.xyz"
            }
        ]
    },
    {}....
]
```