import { jsonDataInterface, statsIndInterface, statsInterface } from './contracts.js';

export async function formatter(data: Array<Array<any>>): Promise<Array<jsonDataInterface>> {
	const o: Array<jsonDataInterface> = [];
	data[ 0 ].forEach((val, idx) => {
		o.push({
			refs: data[ 0 ][ idx ][ 0 ],
			hash: data[ 0 ][ idx ][ 1 ],
			hashAbbrev: data[ 0 ][ idx ][ 2 ],
			tree: data[ 0 ][ idx ][ 3 ],
			treeAbbrev: data[ 0 ][ idx ][ 4 ],
			parents: data[ 0 ][ idx ][ 5 ],
			parentsAbbrev: data[ 0 ][ idx ][ 6 ],
			author: {
				name: data[ 0 ][ idx ][ 7 ],
				email: data[ 0 ][ idx ][ 8 ],
				timestamp: data[ 0 ][ idx ][ 9 ]
			},
			committer: {
				name: data[ 0 ][ idx ][ 10 ],
				email: data[ 0 ][ idx ][ 11 ],
				timestamp: data[ 0 ][ idx ][ 12 ]
			},
			subject: data[ 0 ][ idx ][ 13 ],
			body: data[ 0 ][ idx ][ 14 ],
			notes: data[ 0 ][ idx ][ 15 ],
			stats: formatStats(data[ 1 ][ idx ])
		});
	});
	return o;
}

export function formatStats(data: Array<Array<any>>): statsInterface {
	const s: Array<statsIndInterface> = [];
	data.forEach((val, idx) => {
		s.push({
			additions: data[ idx ][ 0 ],
			deletions: data[ idx ][ 1 ],
			file: data[ idx ][ 2 ]
		});
	});
	return s;
}