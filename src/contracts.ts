export interface jsonDataInterface {
    refs: strLstInterface;
    hash: string;
    hashAbbrev: string;
    tree: string;
    treeAbbrev: string;
    parents: strLstInterface;
    parentsAbbrev: strLstInterface;
    author: personInterface;
    committer: personInterface;
    subject: string;
    body: string;
    notes: string;
    stats: statsInterface;
}

interface strLstInterface {
    [ index: number ]: string;
}

export interface personInterface {
    name: string;
    email: string;
    timestamp: number;
}

export interface statsInterface {
    [ index: number ]: statsIndInterface;
}

export interface statsIndInterface {
    additions: number;
    deletions: number;
    file: string;
}

export interface cleanOptsInterface {
    keys?: Array<string>;
    sanitize?: 'light' | 'heavy' | 'custom';
    sanitizeRule?: RegExp;
    sanitizeReplace?: string;
}
