export const gitFormatMapping = {
	refs: '%d',
	hash: '%H',
	hashAbbrev: '%h',
	tree: '%T',
	treeAbbrev: '%t',
	parents: '%P',
	parentsAbbrev: '%p',
	authorName: '%an',
	authorEmail: '%ae',
	authorTimestamp: '%at',
	committerName: '%cn',
	committerEmail: '%ce',
	committerTimestamp: '%ct',
	subject: '%s',
	body: '%b',
	notes: '%N'
};

export const lightSanitizeRules: (string | RegExp)[][] = [
	[ '"', '<q>' ],
	[ '\\', '\\\\' ]
];

export const heavySanitizeRules: (string | RegExp)[][] = [
	// eslint-disable-next-line no-control-regex
	[ /[\x00\x08\x0B\x0C\x0E-\x1F]/g, '' ]
];

export const sanitizeKeys = [
	'subject',
	'body',
	'notes'
];

export let loggingPrefix = '';

export function setLoggingPrefix(value: string): void {
	loggingPrefix = value;
}

export let loggingEnabled = true;

export function setLoggingEnabled(value: boolean): void {
	loggingEnabled = value;
}