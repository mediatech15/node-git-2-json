import { cleaner } from './cleaner.js';
import { cleanOptsInterface } from './contracts.js';
import { formatter } from './formatter.js';
import { gitLog, handleOutput } from './git.js';
import { parser } from './parser.js';

export {
	gitLog,
	handleOutput,
	getFormat
} from './git.js';
export {
	parser,
	parseNumbers,
	parseRefs,
	splitCommit,
	splitStat,
	parseParents,
	parseTimestamp
} from './parser.js';
export {
	cleaner,
	removeKeys,
	sanitizeStrings
} from './cleaner.js';
export {
	gitFormatMapping,
	lightSanitizeRules,
	heavySanitizeRules,
	sanitizeKeys,
	loggingEnabled,
	loggingPrefix,
	setLoggingEnabled,
	setLoggingPrefix
} from './constants.js';
export {
	ProcessingError
} from './error.js';
export {
	jsonDataInterface,
	personInterface,
	statsInterface,
	cleanOptsInterface
} from './contracts.js';
export {
	formatter,
	formatStats
} from './formatter.js';

export async function generateJson(repo: string, count?: number, output?: 'JSON' | 'Object', clean?: cleanOptsInterface): Promise<string>;
export async function generateJson(repo: string, count?: number, output?: 'JSON' | 'Object', clean?: cleanOptsInterface): Promise<object>;
export async function generateJson(repo: string, count?: number, output?: 'JSON' | 'Object', clean?: cleanOptsInterface): Promise<any> {
	count = count == undefined ? -1 : count;
	output = output == undefined ? 'JSON' : output;
	clean = clean == undefined ? {} : clean;
	const data = await gitLog(repo, count);
	const indData = await handleOutput(data[ 0 ], data[ 1 ]);
	const parseData = await parser(indData);
	const formattedData = await formatter(parseData);
	let cleanedData: object;
	if (Object.keys(clean).length != 0) {
		cleanedData = await cleaner(formattedData, clean);
	} else {
		cleanedData = formattedData;
	}
	if (output == 'JSON') {
		return JSON.stringify(cleanedData);
	} else if (output == 'Object') {
		return cleanedData;
	}
}
