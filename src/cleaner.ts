import { heavySanitizeRules, lightSanitizeRules, sanitizeKeys } from './constants.js';
import { cleanOptsInterface, jsonDataInterface } from './contracts.js';

export async function cleaner(data: Array<jsonDataInterface>, cleanRules: cleanOptsInterface): Promise<object> {
	let c: Array<object> = data;
	if (cleanRules.keys != null) {
		c = removeKeys(data, cleanRules.keys);
	}
	if (cleanRules.sanitize != null) {
		if (cleanRules.sanitize == 'custom') {
			if (cleanRules.sanitizeRule == null) {
				throw new Error('Custom sanitize rule not provided with "custom" flag');
			} else if (!(cleanRules.sanitizeRule instanceof RegExp) || typeof (cleanRules.sanitizeReplace != 'string')) {
				throw new Error('Custom rule types for one of or both are incorrect. "sanitizeRule" or "sanitizeReplace"');
			}
		}
		c = sanitizeStrings(c, cleanRules);
	}
	return c;
}

export function removeKeys(data: Array<object>, keys: Array<string>): Array<object> {
	const tempData = data;
	keys.forEach((val) => {
		data.forEach((dval, didx) => {
			try {
				delete tempData[ didx ][ val ];
			} catch (err) {
				console.warn(`key: ${val} not found in object ${dval}`);
			}
		});
	});
	return tempData;
}

export function sanitizeStrings(data: Array<object>, cleanRules: cleanOptsInterface): Array<object> {
	const tempData = data;
	data.forEach((val, idx) => {
		if (cleanRules.sanitize == 'light') {
			sanitizeKeys.forEach((sval) => {
				if (sval in val) {
					lightSanitizeRules.forEach((rval) => {
						val[ sval ].replace(rval[ 0 ], rval[ 1 ]);
					});
				}
			});
		} else if (cleanRules.sanitize == 'heavy') {
			sanitizeKeys.forEach((sval) => {
				if (sval in val) {
					heavySanitizeRules.forEach((rval) => {
						val[ sval ].replace(rval[ 0 ], rval[ 1 ]);
					});
				}
			});
		} else if (cleanRules.sanitize == 'custom') {
			sanitizeKeys.forEach((sval) => {
				if (sval in val) {
					val[ sval ].replace(cleanRules.sanitizeRule, cleanRules.sanitizeReplace);
				}
			});
		}
	});
	return tempData;
}