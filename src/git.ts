import { spawn } from 'child_process';
import { gitFormatMapping, loggingEnabled, loggingPrefix } from './constants.js';
import { ProcessingError } from './error.js';

export async function gitLog(repo: string, count: number): Promise<Array<string>> {
	return new Promise((resolve, reject) => {
		let stdout = '';
		let stderr = '';
		const args = [
			'-C',
			repo,
			'log',
			'--numstat',
			'--date-order',
			'--all',
			getFormat(),

		];
		if (getNumber(count) != '') {
			args.push(getNumber(count));
		}
		const g = spawn('git', args);
		g.stdout.on('data', d => {
			stdout += d;
		});

		g.stderr.on('data', d => {
			stderr += d;
		});
		g.on('error', (err) => {
			console.error(err);
			reject(err);
		});
		g.on('close', (code) => {
			if (code !== 0) {
				console.error(`Git exited with code ${code}`);
				reject(code);
			} else {
				resolve([ stdout, stderr ]);
			}
		});
	});
}

export function getFormat(): string {
	const base = '--pretty=format:%x02###%x02';
	const k = Object.keys(gitFormatMapping);
	const v = k.map(i => gitFormatMapping[ i ]).join('%x00');
	return base.replace('###', v);
}

function getNumber(num: number): string {
	if (num == -1) {
		return '';
	} else if (num >= 0) {
		return `-n ${num}`;
	} else {
		throw new RangeError('The count must be a positive number or -1 for all');
	}
}

export async function handleOutput(out: string, err: string): Promise<Array<Array<string>>> {
	return new Promise((resolve, reject) => {
		const dataChunk = out.split('\u0002').slice(1);
		const commits = dataChunk.filter((val, idx) => (idx + 1) % 2);
		const stats = dataChunk.filter((val, idx) => idx % 2);
		if (commits.length !== stats.length) {
			throw new ProcessingError(`Data returned was invalid.\nCommits Length: ${commits.length}\nStats Length: ${stats.length}`);
		}
		if (loggingEnabled) {
			console.log(`${loggingPrefix}The repository returned ${commits.length} commits`);
		}
		resolve([ commits, stats ]);
	});
}