export async function parser(data: Array<Array<string>>): Promise<Array<Array<any>>> {
	const commits = data[ 0 ];
	const stats = data[ 1 ];
	const splitCommits: Array<any> = [];
	commits.forEach((val) => {
		splitCommits.push(splitCommit(val));
	});
	const splitStats: Array<Array<Array<any>>> = [];
	stats.forEach((val) => {
		splitStats.push(splitStat(val));
	});
	splitCommits.forEach((val, idx) => {
		splitCommits[ idx ][ 0 ] = parseRefs(val[ 0 ]);
		splitCommits[ idx ][ 5 ] = parseParents(val[ 5 ]);
		splitCommits[ idx ][ 6 ] = parseParents(val[ 6 ]);
		splitCommits[ idx ][ 9 ] = parseTimestamp(parseNumbers(val[ 9 ]));
		splitCommits[ idx ][ 12 ] = parseTimestamp(parseNumbers(val[ 12 ]));
	});
	splitStats.forEach((val, idx) => {
		val.forEach((ival, iidx) => {
			splitStats[ idx ][ iidx ][ 0 ] = parseTimestamp(parseNumbers(ival[ 0 ]));
			splitStats[ idx ][ iidx ][ 1 ] = parseTimestamp(parseNumbers(ival[ 1 ]));

		});
	});
	return [ splitCommits, splitStats ];
}

export function splitCommit(commit: string): Array<string> {
	const c = commit.split('\u0000');
	c.forEach((val, idx) => { c[ idx ] = val.trim(); });
	return c;
}

export function splitStat(stat: string): Array<Array<string>> {
	const s = stat.trim().split('\n');
	const t: Array<Array<string>> = [];
	s.forEach((val, idx) => { t[ idx ] = val.split('\t'); });
	return t;
}

export function parseRefs(ref: string): Array<string> {
	if (ref.toString() != '') {
		return ref.toString().substring(1, ref.length - 1).replace('->', ',').split(', ');
	} else {
		return [];
	}
}

export function parseParents(parent: string): Array<string> {
	return parent.split(' ');
}

export function parseTimestamp(stamp: number): number {
	return stamp * 1000;
}

export function parseNumbers(ref: string): number {
	return parseInt(ref);
}