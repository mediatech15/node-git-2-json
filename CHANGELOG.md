#### [v1.10.0](https://gitlab.com/mediatech15/node-git-2-json/compare/v1.10.0...v1.10.0)

#### [v1.10.0](https://gitlab.com/mediatech15/node-git-2-json/compare/v1.9.0...v1.10.0)

> 31 October 2022

- fixed un-neccesary exports [`71af76f`](https://gitlab.com/mediatech15/node-git-2-json/commit/71af76f87bc4c47f80addd05c91072ce42d5e26f)
- CI RELEASE [`30c93a1`](https://gitlab.com/mediatech15/node-git-2-json/commit/30c93a19dd3fbd580ccc6f61d3ce0175f315b9e0)

#### [v1.9.0](https://gitlab.com/mediatech15/node-git-2-json/compare/v1.8.0...v1.9.0)

> 30 October 2022

- added set funcs for rules [`e39cbe9`](https://gitlab.com/mediatech15/node-git-2-json/commit/e39cbe90cae2211cc33e3c17825f447b1efff3f3)
- CI RELEASE [`6e99970`](https://gitlab.com/mediatech15/node-git-2-json/commit/6e9997034528008b388327e83123e4bc574aaf6c)
- added global exports for new functions [`032df95`](https://gitlab.com/mediatech15/node-git-2-json/commit/032df954b0d5290623d48ba93f53eec820772c84)

#### [v1.8.0](https://gitlab.com/mediatech15/node-git-2-json/compare/v1.7.0...v1.8.0)

> 30 October 2022

- CI RELEASE [`f869961`](https://gitlab.com/mediatech15/node-git-2-json/commit/f8699610cb7c2e72cb60898bac1e094f3741a9ae)
- fix const vs let for exported vars that can be overridden [`06e91ea`](https://gitlab.com/mediatech15/node-git-2-json/commit/06e91ea0d357385171e6e9ead438e601c13949ca)

#### [v1.7.0](https://gitlab.com/mediatech15/node-git-2-json/compare/v1.2.1...v1.7.0)

> 30 October 2022

- add ci, add looging constants [`c4c5399`](https://gitlab.com/mediatech15/node-git-2-json/commit/c4c5399be21ebcec667fc0105e00a3d98584b2cc)
- add changelog template [`5725e7b`](https://gitlab.com/mediatech15/node-git-2-json/commit/5725e7b19a7e490122d26de410b929efcfecd5e2)
- Add LICENSE [`7c366dc`](https://gitlab.com/mediatech15/node-git-2-json/commit/7c366dcca90713fdffc18caaae8a005fe4a1aa68)
- fixing ci [`5893695`](https://gitlab.com/mediatech15/node-git-2-json/commit/5893695a8bce270fe10cc990a5b12515b385bbb1)
- fixing ci [`b5d2ad9`](https://gitlab.com/mediatech15/node-git-2-json/commit/b5d2ad9eebd249a4d586346d286a1474ec41f0a0)
- fixing ci [`521a8d7`](https://gitlab.com/mediatech15/node-git-2-json/commit/521a8d72d0a9b30b5e39ec8c68d7e0d871dd2341)
- update ci to build changelog [`868a7fd`](https://gitlab.com/mediatech15/node-git-2-json/commit/868a7fd5ecbaaa291fdadb37c2d485fd7a7361c8)
- fix version from ci issues [`92da01e`](https://gitlab.com/mediatech15/node-git-2-json/commit/92da01edee2d1fd0c76a880444ddcfbfae080f8f)
- fixing ci [`7949fe5`](https://gitlab.com/mediatech15/node-git-2-json/commit/7949fe5105863ba82cbd5cbacab79b2923479c23)
- fix version [`de94dd6`](https://gitlab.com/mediatech15/node-git-2-json/commit/de94dd6606108aaa498a72bed605d3655f995f80)
- Update CHANGELOG.md [`e807879`](https://gitlab.com/mediatech15/node-git-2-json/commit/e8078790b4c7180e8d719a8080fdab9f1dced7a6)
- Update CHANGELOG [`e52fb84`](https://gitlab.com/mediatech15/node-git-2-json/commit/e52fb844f2e7e7d3c3272bc713ca121e693b0a7a)

#### [v1.2.1](https://gitlab.com/mediatech15/node-git-2-json/compare/v1.2.0...v1.2.1)

> 3 July 2022

- added change.md [`fde7b7e`](https://gitlab.com/mediatech15/node-git-2-json/commit/fde7b7e7a5b7e6a9483c2b9a0f25a9bc1c936695)
- fixed typos in readme [`6ce9432`](https://gitlab.com/mediatech15/node-git-2-json/commit/6ce94329374d089ae1f173bc2e3d196792dcacfb)
- update package json to include all md files [`05a91c9`](https://gitlab.com/mediatech15/node-git-2-json/commit/05a91c90b3e4796842372090f0a6aa8d17333fef)

#### [v1.2.0](https://gitlab.com/mediatech15/node-git-2-json/compare/v1.1.0...v1.2.0)

> 2 July 2022

- fixed test and updated readme [`bad5d4d`](https://gitlab.com/mediatech15/node-git-2-json/commit/bad5d4dc1fafe9711c9faf0c23412ea756019543)
- fix test and git command [`b295a47`](https://gitlab.com/mediatech15/node-git-2-json/commit/b295a47c8c9a6a51629685667e6fd33838227350)

#### v1.1.0

> 2 July 2022

- update for packaging [`f2bded7`](https://gitlab.com/mediatech15/node-git-2-json/commit/f2bded7ca234e49d8c1d35347b319420a8ae3a49)
- init commit [`6f35f2f`](https://gitlab.com/mediatech15/node-git-2-json/commit/6f35f2fa4998430c7f31e06decb808d64eeaf63b)
- Initial commit [`8a9a233`](https://gitlab.com/mediatech15/node-git-2-json/commit/8a9a2332322aa3960224f76aa2bc4e28c75c222e)
- fix version [`7d87b67`](https://gitlab.com/mediatech15/node-git-2-json/commit/7d87b6738385e6f8bd2dddd510c894ae3fcb82d6)
