# About
Simple tool to get a JSON from your git log. Inspired by @fabien0102/git2json

# Use-Cases
- gitgraph.js
- analytics
- repository history
- problem tracking

# Basic Usage
```typescript
import { generateJson } from 'node-git-2-json';

let json = await generateJson('path-to-repository');
```
By default your will be returned the entire history as a JSON string.

# Intermediate Usage
```typescript
// To return less history
let json = await generateJson('path-to-repository', 50);
// Returns only last 50 commits as JSON string

// To return a JS/TS Object
let json = await generateJson('path-to-repository', -1, 'Object');
// Returns all history as an Object
```
**Note**: If you need to specify all commits you can pass `-1` for all. 

**Note2**: The last `n` commits is across all branches not currently checked out branch.

# Advanced Usage
```typescript
let cleanOptions = {
	keys: [
		'body',
		'notes'
	]
}
let json = await generateJson('path-to-repository', -1, 'Object', cleanOptions);
// Returns an Object for the entire history and
// removes the "body" and "notes" keys from every commit

let cleanOptions = {
	sanitize: 'light'
}
let json = await generateJson('path-to-repository', -1, 'Object', cleanOptions);
// Returns an Object for the entire history and
// performs a light sanitize on strings known to contain problematic characters. 

let cleanOptions = {
	sanitize: 'heavy'
}
let json = await generateJson('path-to-repository', -1, 'Object', cleanOptions);
// Returns an Object for the entire history and
// performs a heavy sanitize on strings known to contain problematic characters. 
// Warning this is highly aggressive and basically removes all non-printable characters.

let cleanOptions = {
	sanitize: 'custom',
	sanitizeRule: /[0-9]/g,
	sanitizeReplace: '!'
}
let json = await generateJson('path-to-repository', -1, 'Object', cleanOptions);
// Returns an Object for the entire history and
// performs a custom sanitize on strings known to contain problematic characters. 
// This uses a regex supplied by the user and replacement string
// Will in this case replace every digit with !
```

# generateJson()

This is the only method needed in 95% of use. Helper functions are exported for extension but only needed if in specific use or modification is needed.

**Returns**: `string|Array<object>`

**Parameters**: `repo: string, count?: number, output?: 'JSON | Object', clean?: cleanOptsInterface`

|parameter|default|value|required|about|
|---|---|---|---|---|
|`repo`|N/A|`string`|yes|string of path to git repository (can be `./`)|
|`count`|`-1`|`int` (>-1)|no|number of commits in history to use. `-1` is all|
|`output`|`JSON`|`JSON`, `Object`|no|type of output to return|
|`clean`|`{}`|`cleanOptsInterface`|no|options for post-processing of data/cleaning|

# cleanOptsInterface

**type**: `object`

|key|type|notes|
|---|---|---|
|`keys`|`Array<string>`|top level keys in return data format|
|`sanitize`|`light\|heavy\|custom`| string for level of sanitization|
|`sanitizeRule`|`RegExp`|a global regex to use as replace filter for custom sanitize|
|`sanitizeReplace`|`string`|the replacement string for custom rule|

All options are optional but if you choose custom for `sanitize` then you will recieve an error if you dont provide `sanitizeRule` or `sanitizeReplace`

# git json format
```json
[
    {
        "refs": [
            "tag: 1.0.0-b.4",
            "origin/dvt.xyz"
        ],
        "hash": "4c106c65ef3a92e5760fe7379300f9478853f0f7",
        "hashAbbrev": "4c106c65",
        "tree": "2283acd8f4019a7119ac303ed882e5d32424b127",
        "treeAbbrev": "2283acd8",
        "parents": [
            "fe2c48aad4b85311b4a19958559687c70b596f63"
        ],
        "parentsAbbrev": [
            "fe2c48aa"
        ],
        "author": {
            "name": "user",
            "email": "user@email.com",
            "timestamp": 1656708382000
        },
        "committer": {
            "name": "user",
            "email": "user@email.com",
            "timestamp": 1656708382000
        },
        "subject": "jenkins-1.0.0-b.4 [ci skip]",
        "body": "",
        "notes": "",
        "stats": [
            {
                "additions": 1,
                "deletions": 1,
                "file": "package.json"
            },
            {
                "additions": 1,
                "deletions": 1,
                "file": "src/app/test/file.xyz"
            }
        ]
    },
    {}....
]
```